package entity

import "time"

type Message struct {
	Id   int       `json:"id"`
	Date time.Time `json:"date"`
	Text string    `json:"text"`
}
