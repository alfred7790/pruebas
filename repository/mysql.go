package repository

import (
	"sonarcloud/entity"
	"time"
)

func NewMySQLRepository() Repository {
	return &repo{}
}

func (r *repo) GetMessage(m string) (*entity.Message, error) {
	return &entity.Message{
		Id:  int(time.Now().Unix()),
		Date: time.Now().UTC(),
		Text: m,
	}, nil
}
