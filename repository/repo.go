package repository

import "sonarcloud/entity"

type repo struct {}

type Repository interface {
	GetMessage(m string) (*entity.Message, error)
}
