package service

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"sonarcloud/entity"
	"testing"
	"time"
)

type MockRepository struct {
	mock.Mock
}

func (m *MockRepository) GetMessage(msg string) (*entity.Message, error) {
	args := m.Called()
	result := args.Get(0)
	return result.(*entity.Message), args.Error(1)
}

func TestGetMessage(t *testing.T) {
	mockRepo := new(MockRepository)

	message := entity.Message{
		Id:   int(time.Now().Unix()),
		Date: time.Now().UTC(),
		Text: "It's a message",
	}

	mockRepo.On("GetMessage").Return(&message, nil)

	testService := NewMessageService(mockRepo)
	result, _ := testService.CreateMessage("It's a message")

	mockRepo.AssertExpectations(t)

	// Data Assertion
	assert.Equal(t, message.Text, result.Text)
}
