package service

import (
	"sonarcloud/entity"
	"sonarcloud/repository"
)

type service struct {}
var repo repository.Repository

type MessageService interface {
	CreateMessage(m string) (*entity.Message, error)
}

func NewMessageService(repository repository.Repository) MessageService {
	repo = repository
	return &service{}
}

func (s *service) CreateMessage(msg string) (*entity.Message, error) {
	return repo.GetMessage(msg)
}