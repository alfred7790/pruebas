package main

import (
	"fmt"
	"net/http"
	"sonarcloud/controller"
	router "sonarcloud/http"
	"sonarcloud/repository"
	"sonarcloud/service"
)

var (
	repo = repository.NewMySQLRepository()
	msgService = service.NewMessageService(repo)
	userController = controller.NewMessageController(msgService)
	httpRouter = router.NewMuxRouter()
)

func main() {
	const port string = ":8085"

	httpRouter.GET("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "¡this works!")
	})

	// This is a change
	httpRouter.GET("/hello", userController.GetMessage)

	httpRouter.SERVE(port)
}
