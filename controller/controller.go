package controller

import (
	"net/http"
	"sonarcloud/service"
)

type controller struct {}

var messageservice service.MessageService

type MessageController interface {
	GetMessage(w http.ResponseWriter, r *http.Request)
}

func NewMessageController(service service.MessageService) MessageController {
	messageservice = service
	return &controller{}
}